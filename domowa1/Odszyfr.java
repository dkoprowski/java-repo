package szyfrowanie;

import java.io.*;

public class Odszyfr {
	
	static void prt(char litera){
		System.out.print(litera);
	}
	
	public static void main(String[] args) {

		try{
			FileInputStream szyfr = new FileInputStream("szyfr.html");
			FileOutputStream odszyfr = new FileOutputStream("odszyfr.txt");
			FileInputStream klucz = new FileInputStream("klucz.html");

			
			int nrSzyfru=0;
			for (int i = 0; i < 7; i++) {
				if(i!=4)szyfr.read();
				else nrSzyfru=szyfr.read()-48;
			}

			switch(nrSzyfru){
			case 0:
				int litera = szyfr.read();				
					while(litera != -1){
						prt((char)litera);
						odszyfr.write(litera-3);
						litera = szyfr.read();
						litera = szyfr.read();
						odszyfr.write(litera+10);
						litera = szyfr.read();
						litera = szyfr.read();
					}
					litera=0;

			break;
			case 1:
				System.out.print(nrSzyfru);
				
				int litera2, kluczyk;
				
				litera2 = szyfr.read();
				while (litera2!=-1){
					kluczyk = klucz.read();
					odszyfr.write((litera2^kluczyk)^15);
					litera2 = szyfr.read();
				}
			break;
			case 2:
				System.out.print(nrSzyfru);
			break;
			default:
				System.err.println("Wystapil nieoczekiwany blad! - poinformuj autora.");
			break;
		}
			szyfr.close();
			odszyfr.close();
			klucz.close();
		}
		
		catch (IOException e) {
			System.err.println("Nie mozna odnalezc szyfru!");
		}
	}
}
