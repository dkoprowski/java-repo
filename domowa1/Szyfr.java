package szyfrowanie;

import java.io.*;
import java.util.Random;


public class Szyfr {
	static int[] losujSzyfr(){
		int tab[] = new int[7];
		Random generator = new Random();
		for (int i = 0; i < tab.length; i++) {
			tab[i]=generator.nextInt(125);
			if(tab[i]<32) tab[i]+=32;
		}
		tab[4]=generator.nextInt(3);
		return tab;
	}
	static void prt(char litera){
		System.out.print(litera);
	}
	static void prts(String napis){
		System.out.print(napis);
	}
	
	public static void main(String[] args) {
		
		try{
			FileInputStream tekst = new FileInputStream("tekst.txt");
			FileInputStream kluczIn = new FileInputStream("klucz.html");
			FileOutputStream szyfr = new FileOutputStream("szyfr.html");
			FileOutputStream klucz = new FileOutputStream("klucz.html");
			
			//Losowanie szyfru:
			int tab[] = new int[7];
			tab = losujSzyfr();
			for (int i = 0; i < tab.length; i++) {
				if(i!=4)szyfr.write(tab[i]);
				else szyfr.write(tab[i]+48);
				
			}
			int nrSzyfru = tab[4];
			
			//Szyfrowanie jednym z trzech sposobow:
			switch(nrSzyfru){
			case 0:		//Szyfr zwracajacy 2x wiecej znakow niz ma plik szyfrowany
				int pom;
				int litera = tekst.read();
				
					while(litera != -1){
						prt((char)litera);
						szyfr.write(litera+3);
							pom=(litera+litera*4)%126;	//
							if(pom<32) pom=pom+33;		//oszukana
							szyfr.write(pom);			//
						litera = tekst.read();
						prt((char)litera);
						szyfr.write(litera-10);
						szyfr.write(litera-6);			// oszukana
						litera = tekst.read();
					}
				
			break;
			case 1:
				int litera2;
				litera2 = tekst.read();
				int litera3 = tekst.available();
				Random generator = new Random();
				for (int i = 0; i < litera3; i++) {			//Generuję losowy klucz mający tyle samo bajtów co plik
					klucz.write(generator.nextInt(120)); 	//klucz dla XORa
				}
				do {
					szyfr.write((litera2^kluczIn.read())^15);	//XOR tekstu jawnego z kluczem a następnie z piętnastką
					litera2 = tekst.read();
				} while (litera2!=-1);
				
			break;
			case 2:
				prts("Tutaj bedzie szyfr nr 2");
			break;
			default:
				System.err.println("Wystapil nieoczekiwany blad! - poinformuj autora.");
			break;
			}
			tekst.close();
			szyfr.close();
			klucz.close();
			kluczIn.close();
	}
	catch (IOException e) {
		System.err.println("Nie można odnaleźć pliku do szyfrowania!");
	}	
		
	}


		
}
