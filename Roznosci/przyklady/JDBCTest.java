/* podatawowe klasy JDBC */
import java.sql.*;

public class JDBCTest {
	public static void main(String[] args) {

		/* Format: jdbc:postgresql://host/baza */
		String url = "jdbc:postgresql://localhost/test";
		try {
			Class.forName("org.postgresql.Driver");
			Connection db = DriverManager.getConnection(url, "tomek", "123456");
			System.out.println("OK");
		} catch (SQLException e) {
			System.err.println("Wyjatek z zapytania SQL: " + e.getMessage());
			e.printStackTrace(System.err);
		} catch (ClassNotFoundException e) {
			System.err
					.println("Wyjatek przy ladowaniu klas: " + e.getMessage());
			e.printStackTrace(System.err);
		}

	}
}
