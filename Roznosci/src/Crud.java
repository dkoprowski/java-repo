/**
- zrobic CRUDa (stworzyc apke ktora dla danych bedzie umozliwiala
tworzenie, update, i kasacje bazdy danych)
*/

import java.sql.*;
import java.io.*;
import java.util.*;

class Crud {
	
	/**
	   Nawiązuje połączenie, korzystając
	   z właściwości w pliku database.properties
	*/
	public static Connection getConnection()
	   throws SQLException, IOException
	{  
	   Properties props = new Properties();
	   FileInputStream in = new FileInputStream("database.properties");
	   props.load(in);
	   in.close();

	   String drivers = props.getProperty("jdbc.drivers");
	   if (drivers != null)
	      System.setProperty("jdbc.drivers", drivers);
	   String url = props.getProperty("jdbc.url");
	   String username = props.getProperty("jdbc.username");
	   String password = props.getProperty("jdbc.password");

	//   System.out.println("baza danych:\t"+props.getProperty("jdbc.url"));
	//   System.out.println("uzytkownik:\t"+props.getProperty("jdbc.username")+"\n");

	   return DriverManager.getConnection(url, username, password);
	}
	
	/**
	   Wykonuje test polegający na utworzeniu tabeli, 
	   wstawieniu do niej wartości, prezentacji zawartości, 
	   usunięciu tabeli.
	*/
/**
	public static void runTest()
	   throws SQLException, IOException
	{
	   Connection conn = getConnection();
	   try
	   {
	      Statement stat = conn.createStatement();
	      
	      stat.execute("CREATE TABLE Greetings (Message CHAR(20));");
	      stat.execute("INSERT INTO Greetings VALUES ('Hello, World!');");
	      
	      ResultSet result = stat.executeQuery("SELECT * FROM Greetings;");
	      result.next();
	      System.out.println(result.getString(1));
	   //   stat.execute("DROP TABLE Greetings;");      
	   }
	   finally
	   {
	      conn.close();
	   }
	}
*/
	public static void utworz(String nazwa) throws SQLException, IOException{
		Connection conn = getConnection();
		Statement stat = conn.createStatement();
		stat.execute("CREATE TABLE "+nazwa+" (imie char(20),nazwisko char(30));");
	}
	
	public static void dodaj(String nazwa, String imie,String nazwisko) throws SQLException, IOException{
		Connection conn = getConnection();
		Statement stat = conn.createStatement();
		stat.execute("INSERT INTO "+nazwa+" VALUES ('"+imie+"'"+",'"+nazwisko+"');");
	}
	
	public static void usun(String nazwa) throws SQLException, IOException{
		Connection conn = getConnection();
		Statement stat = conn.createStatement();
		stat.execute("DROP TABLE "+nazwa+";");
	}
	
	public static void wyswietl(String nazwa) throws SQLException, IOException{
		Connection	conn = getConnection();
		Statement	stat = conn.createStatement();
	    ResultSet	result = stat.executeQuery("SELECT * FROM "+nazwa+";");
	    while(result.next()){
	    System.out.println(result.getString(1)+"\t"+result.getString(2));
	    }
	}
	
	public static void update(String nazwa, String imie,String imie1) throws SQLException, IOException{
		Connection	conn = getConnection();
		Statement	stat = conn.createStatement();
		String str = ("update "+nazwa+" set imie=\""+imie+"\" where imie=\""+imie1+"\"");
	    int	result = stat.executeUpdate(str);
	    System.err.println("Updatowano "+result+" wiersz/e/y");
	    
	    wyswietl(nazwa);
	}
	
	public static void main (String args[])
	{  
	   try
	   {  
		  utworz("osoba");
	      dodaj("osoba","Alojzy","Przykladowski");
	      dodaj("osoba","Mirek","Kochanowski");
	      dodaj("osoba","Mirek","Bazodanowy");
	      dodaj("osoba","Edyta","Maluszkiewicz");
	      wyswietl("osoba");
	      update("osoba","Danny","Mirek");
	   }
	   catch (SQLException ex)
	   {  
	      while (ex != null)
	      {  
	         ex.printStackTrace();
	         ex = ex.getNextException();
	      }
	   }
	   catch (IOException ex)
	   {  
	      ex.printStackTrace();
	   }
	   try {
		usun("osoba");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	

} // /Crud