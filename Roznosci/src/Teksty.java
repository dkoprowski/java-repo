import java.io.*;


public class Teksty {

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(
				new FileReader("names.txt")
				);
		PrintWriter pw = new PrintWriter(
				new FileWriter("names2.txt",true)
				);
		
		String linia;
		while((linia = br.readLine()) != null)
			pw.println(linia.toLowerCase());
		
		pw.close();
		br.close();
		
		System.out.println("OK");
	}
}
