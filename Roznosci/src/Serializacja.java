import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Uczen implements Serializable{
	String imie;
	String nazwisko;
	int wiek;
	int klasa;
	
	public Uczen(String imie,String nazwisko,int wiek,int klasa){
		this.imie=imie;
		this.nazwisko=nazwisko;
		this.wiek=wiek;
		this.klasa=klasa;
	}
}

public class Serializacja {

	
	
	public static void serializacja(Uczen uczen) throws FileNotFoundException, IOException{
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File("uczen.object")));
		out.writeObject(uczen);
		out.close();
	}
	
	public static void deserializacja() throws FileNotFoundException, IOException, ClassNotFoundException{
		Uczen przywrocony;
		ObjectInputStream in = new ObjectInputStream(new FileInputStream("uczen.object"));
		przywrocony = (Uczen)in.readObject();
		in.close();
		System.out.println("Przywrocono ucznia "+przywrocony.imie+" "+przywrocony.nazwisko);
	}
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException{
		Uczen danny = new Uczen("Danny","Bizarri",20,2);
		serializacja(danny);
		deserializacja();
	}

}
