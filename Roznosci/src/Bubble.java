import java.util.Scanner;
public class Bubble {
	static void prt(String s){
		System.out.println(s);
	}

	static int[] bubble(int[] A, int n){
		do{
			for(int i=0;i<A.length-1;i++){
				if(A[i]<A[i+1]){
					int pom = A[i];
					A[i]=A[i+1];
					A[i+1]=pom;
				}
			}
			n--;
		}while(n>1);
		return A;
	}
	
	
	static int binarne(int[] A, int y){
		int index=-1, srodek,x, l, p;
		l=0;	p=A.length-1;
			while(l<=p){
				srodek=(l+p)/2;
				x=A[srodek];
				if(x==y){
					index = srodek;
					l = p+1;
				}
				if(x<y) l=srodek+1;
				else p=srodek-1;
			}
		return index;
	}
	
	public static void main(String[] args) {
		int[] tab = {9,5,5,3,2,4,6,8,7,1,0,-1,15,4,32,65,11,43};
		for (int i = 0; i < tab.length; i++) System.out.print(tab[i]+" ");
		prt("");
		tab = bubble(tab, tab.length);	
		for (int i = 0; i < tab.length; i++) System.out.print(tab[i]+" ");
		int n=7;
		prt("\nszukana wartosc to "+n);
		n=binarne(tab,n);
		prt("indeks nr "+n);}}
