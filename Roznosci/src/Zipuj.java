import java.io.*;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.*;


public class Zipuj {
	
	private static void copyFile(OutputStream out, InputStream in) throws Exception {
		
		// isContent has a one byte which represents number of 0 - 255
		// or if in.read() return no bytes, it has -1 number; 
		int isContent = in.read(); 
		
		while(isContent != -1) {
			out.write(isContent);
			isContent = in.read();
		}
		
		in.close();
	}
	
	static void compress() throws IOException{
		int dlugosc, a=1;
		ZipOutputStream archw = new ZipOutputStream(new FileOutputStream("archiwum.zip"));
		
		//Pobieranie nazw plikow do tablicy
		System.out.print("Ile plikow chcesz skompresowac?  ");
		Scanner in = new Scanner(System.in);
		dlugosc = in.nextInt();
		String[] nazwa =new String[dlugosc];
		for(int i=0;i<dlugosc;i++) {
			System.out.print("Podaj nazwe pliku z rozszerzeniem:  ");
			nazwa[i] = in.next();	//nextLine() wywala blad - why?
		}
		
		//Dodawanie plikow do archiwum
		for(int i=0;i<dlugosc;i++){
			try{
				ZipEntry plik = new ZipEntry(nazwa[i]);
				FileInputStream input = new FileInputStream(nazwa[i]);	
				
					archw.putNextEntry(plik);
					for(int c=input.read();c!= -1; c=input.read()){
						archw.write(c);
					}		
				input.close();
			}
			catch (IOException e) {
				a=0;
				System.err.println(" --- Nie ma takiego pliku! ---");
			}
			
		}
		
		if(a!=0) 	System.err.println("\n--- Skompresowano pliki ---");
		archw.close();
	}
	
	static void extract(ZipFile plik) throws IOException{
		Enumeration e = plik.entries();
		ZipEntry wypakowywany = null;
		
		while(e.hasMoreElements()){
			wypakowywany = (ZipEntry) e.nextElement();
			FileOutputStream nowy_plik = new FileOutputStream(wypakowywany.getName());
			BufferedOutputStream bos = new BufferedOutputStream(nowy_plik);
			
			try {
				copyFile(bos,plik.getInputStream(wypakowywany));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			bos.close();
			nowy_plik.close();
			System.err.println("Wypakowano plik: "+wypakowywany);
			
		}
		
	}

	
	public static void main(String[] args) throws IOException {
		
		//wybor opcji
		int wybor;
		System.out.print(" 1 -Skompresuj pliki\n 2 -Wypakuj Pliki\n 0 -Wyjdz\nCo chcesz zrobic?:  ");
		Scanner in = new Scanner(System.in);
		wybor = in.nextInt();
		System.out.println();
	
		//menu
		switch(wybor){
		case 1: 	compress();
		break;
		case 2:		
			String nazwa;
			System.out.println("Podaj nazwe pliku do odzipowania:");
			nazwa = in.next();
		//	FileInputStream input = new FileInputStream(nazwa);
			ZipFile input = new ZipFile(nazwa);
			extract(input);
		break;
		default:	break;
		}

	

	}
}


